# The GitKit

This is the development repository for the GitKit-FarmData2 repository used by the GitKit materials. This is where we do development on the GitKit-FarmData2 repository and the code to deploy it for your class.

If you are interested in using the GitKit Text in a course please see the Instructor Guide in the text on [Runestone Academy](https://runestone.academy/) by using one of the following links:
- [GitKit (Linux Desktop Edition)](https://runestone.academy/ns/books/published/gitkitlinux/the-gitkit-book.html?mode=browsing) - students work in a full Linux desktop environment in this edition.
- [GitKit (VSCode Edition)](https://runestone.academy/ns/books/published/gitkitvscode/the-gitkit-book.html?mode=browsing) - students work within the VSCode IDE in this edition.

## Deprecated Instructor Guide

If you arrived here looking for the Instructor Guide, because this is where it used to live, that Instructor Guide is now deprecated.  However, it is still available [here](DeprecatedInstructorGuide.md) and should still work. However, as the new GitKit Runestone text continues to evolve, the deprecated version of the Instructor Guide will eventually be removed.
